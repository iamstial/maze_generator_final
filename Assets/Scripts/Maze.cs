﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze : MonoBehaviour
{

    public int rows = 5; // dimensions of Grid (rows)
    public int columns = 5; //      >>            (columns)


    public GameObject Wall; // initialize Wall
    public GameObject Floor; //   >>      Floor

    public TheCell[,] grid; // grid of 2dimensional array cell-type 

    public int currentRow; // where is our player in terms of rows?
    public int currentColumn; //          >>           >>     columns?

    public bool scanComplete; // completed scan indicator

    void Start()
    {

        GenerateGrid();

    }

    void GenerateGrid()
    {
        
            // destroy all the children of this transform object.
            foreach (Transform transform in transform)
            {
                Destroy(transform.gameObject);
            }

            // first, we create the grid with all the walls and floors.
            createMazeGrids();

            
            

            // reset the algorithm variables.
            currentRow = 0;
            currentColumn = 0;
            scanComplete = false;

            // then we run the algorithm to carve the paths.
            HuntAndLock();
        
    }
    void createMazeGrids()
    {
        float size = Wall.transform.localScale.x; // set size
        grid = new TheCell[rows,
            columns]; // creation of grid based on Cells which have a reference to all the walls surrounding them

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {

                GameObject floor = Instantiate(Floor, new Vector3(j * size, 0, -i * size), Quaternion.identity); // run 4 columns x 4 rows , a total of 16 times instantiating the floor on the appropriate position given. I also put the z axis on the negative side so the top left and top bottom floor parts correspond with the scene camera.floor.name = "Floor " + i + "_" + j; // each floor object gets a name on runtime , based on it's location
                floor.GetComponent<MeshRenderer>().material.color = new Color(0.65f, 0.12f, 0.22f, 1f);

                GameObject upWall = Instantiate(Wall, new Vector3(j * size, 1.75f, -i * size + 1.25f), Quaternion.identity); //hardcoded solution, it would be better to calculate dimensions here but I don't have neither the time nor the knowledge for it.
                upWall.name = "UpWall " + i + "_" + j; // each upper wall object gets a name on runtime , based on it's location
                upWall.GetComponent <MeshRenderer>(). material.color = new Color(Random.Range(0f,1f), Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)); // colourize every upWall randomly


                GameObject downWall = Instantiate(Wall, new Vector3(j * size, 1.75f, -i * size - 1.25f), Quaternion.identity); // same situation us upwall
                downWall.name = "DownWall " + i + "_" + j; // each down wall object gets a name on runtime , based on it's location
                downWall.GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)); // colourize every downWall randomly

                GameObject leftWall = Instantiate(Wall, new Vector3(j * size - 1.25f, 1.75f, -i * size), Quaternion.Euler(0, 90, 0)); // exact same procedure as above but now with rotation due to having to deal with side walls
                leftWall.name = "LeftWall " + i + "_" + j; // each left wall object gets a name on runtime, based on it's location
                leftWall.GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)); // colourize every leftWall randomly

                GameObject rightWall = Instantiate(Wall, new Vector3(j * size + 1.25f, 1.75f, -i * size), Quaternion.Euler(0, -90, 0)); //    same situation as upwall
                rightWall.name = "RightWall " + i + "_" + j; // each right wall object gets a name on runtime, based on it's location
                rightWall.GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)); // colourize every rightWall randomly


                //create the maze cell and add references to it's walls
                grid[i, j] = new TheCell();

                grid[i, j].UpWall = upWall;
                grid[i, j].RightWall = rightWall;
                grid[i, j].LeftWall = leftWall;
                grid[i, j].DownWall = downWall;


                // put all maze components under the same parent in hierarchy
                floor.transform.parent = transform; 
                upWall.transform.parent = transform;   
                downWall.transform.parent = transform;   
                leftWall.transform.parent = transform;   
                rightWall.transform.parent = transform;   


                
                if (i == rows - 1 && j == columns - 1)    //create the entrance
                {
                    Destroy(rightWall);
                }

                
                if (i == 0 && j == 0)    // create the exit
                {
                    Destroy(leftWall);
                }

            }

        }

    }

    // destroyer functions
    void DestroyUp()
    {
        if (grid[currentRow, currentColumn].UpWall)
        {
            Destroy(grid[currentRow, currentColumn].UpWall);
        }
    }

    void DestroyDown()
    {
        if (grid[currentRow, currentColumn].DownWall)
        {
            Destroy(grid[currentRow, currentColumn].DownWall);
        }
    }

    void DestroyLeft()
    {
        if (grid[currentRow, currentColumn].LeftWall)
        {
            Destroy(grid[currentRow, currentColumn].LeftWall);
        }
    }

    void DestroyRight()
    {
        if (grid[currentRow, currentColumn].RightWall)
        {
            Destroy(grid[currentRow, currentColumn].RightWall);
        }
    }

    bool VisitedNeighborCellsExist(int row, int column)
    {
        // upper check
        if (row > 0 && grid[row - 1, column].IsVisited)
        {
            return true;
        }

        // down check
        if (row < rows - 1 && grid[row + 1, column].IsVisited)
        {
            return true;
        }

        // left check
        if (column > 0 && grid[row, column - 1].IsVisited)
        {
            return true;
        }

        // right check
        if (column < columns - 1 && grid[row - 1, column].IsVisited)
        {
            return true;
        }

        return false;
    }




    //check to see if there are unvisited cells through this function
    bool UnvisitedNeighborCellsExist()
    {
        //up check
        if (isCellUnVisitedAndInBounds(currentRow - 1, currentColumn))
        {
            return true;
        }

        //down check
        if (isCellUnVisitedAndInBounds(currentRow + 1, currentColumn))
        {
            return true;
        }

        //left check
        if (isCellUnVisitedAndInBounds(currentRow, currentColumn + 1))
        {
            return true;
        }

        //right check
        if (isCellUnVisitedAndInBounds(currentRow, currentColumn - 1))
        {
            return true;
        }

        return false; //when there is a dead end
    }


    // check visited and bounds 
    bool isCellUnVisitedAndInBounds(int row, int column)
    {
        if (row >= 0 && row < rows && column >= 0 && column < columns && !grid[row, column].IsVisited)
        {
            return true;
        }

        return false;
    }


    void HuntAndLock()
    {
        grid[currentRow, currentColumn].IsVisited = true; //it's the cell the random walk starts from

        while (!scanComplete)
        {
            KeepWalking();
            HuntersNeverRest();
        }


    }

    void KeepWalking()
    {
        while (UnvisitedNeighborCellsExist())
        {
            int direction = Random.Range(0, 4); //start walking into a random possible direction


            if (direction == 0) // check upwards
            {
                if (isCellUnVisitedAndInBounds(currentRow-1, currentColumn)) // make sure the above cell is unvisited and within grid boundaries.

                {

                    if (grid[currentRow, currentColumn].UpWall)
                    {
                        DestroyUp();
                    }

                    // update position
                    currentRow--;

                    //mark grid as isVisited
                    grid[currentRow, currentColumn].IsVisited = true;


                    if (grid[currentRow, currentColumn].DownWall) // destroy the down wall of the cell above if there's any.
                    {
                        DestroyDown();
                    }


                }
            }

            else if (direction == 1) // check downwards
            {
                if (isCellUnVisitedAndInBounds(currentRow + 1, currentColumn)) //make sure the below cell is unvisited and within grid boundaries.
                {
                    if (grid[currentRow, currentColumn].DownWall)
                    {
                        DestroyDown();
                    }

                    currentRow++;

                    grid[currentRow, currentColumn].IsVisited = true;

                    if (grid[currentRow, currentColumn].UpWall) // destroy the up wall of the cell below if there's any.
                    {
                        DestroyUp();
                    }
                }
            }

            else if (direction == 2) // check to the left
            {
                if (isCellUnVisitedAndInBounds(currentRow , currentColumn -1)) // make sure the left cell is unvisited and within grid boundaries.
                {
                    if (grid[currentRow, currentColumn].LeftWall)
                    {
                        DestroyLeft();
                    }

                    currentColumn--;

                    grid[currentRow, currentColumn].IsVisited = true;

                    if (grid[currentRow, currentColumn].RightWall) // destroy the right wall of the cell at the left if there's any.
                    {
                        DestroyRight();
                    }
                }

            }

            else if (direction == 3) // check to the right                  
            {
                if (isCellUnVisitedAndInBounds(currentRow, currentColumn + 1)) // make sure the right cell is unvisited and within grid boundaries.
                {

                    if (grid[currentRow, currentColumn].RightWall)
                    {
                        DestroyRight();
                    }

                    currentColumn++;
                    grid[currentRow, currentColumn].IsVisited = true;

                    if (grid[currentRow, currentColumn].LeftWall) // destroy the left wall of the cell at the right if there's any.
                    {
                        DestroyLeft();
                    }
                }
            }
        }
    }

    void DestroyAdjacentWall()
    {
        bool isDestroyed = false;
        

        while (!isDestroyed)
        {

            // pick a random adjacent cell that is already visited and within boundaries,
            // then destroy the wall or walls between the current cell and adjacent cell.
            int direction = Random.Range(0, 4);

            if (direction == 0)  //check upwards
            {
                if (currentRow > 0 && grid[currentRow - 1, currentColumn].IsVisited)
                {

                    if (grid[currentRow, currentColumn].UpWall)
                    {
                        Destroy(grid[currentRow, currentColumn].UpWall);
                    }

                    if (grid[currentRow - 1, currentColumn].DownWall)
                    {
                        Destroy(grid[currentRow - 1, currentColumn].DownWall);
                    }

                    isDestroyed = true;
                }


            }

            else if (direction == 1)  //check downwards
            {
                if (currentRow < rows -1 && grid[currentRow + 1, currentColumn].IsVisited)
                {
                    if (grid[currentRow, currentColumn].DownWall)
                    {
                        Destroy(grid[currentRow, currentColumn].DownWall);
                    }

                    if (grid[currentRow + 1, currentColumn].UpWall)
                    {
                        Destroy(grid[currentRow + 1, currentColumn].UpWall);
                    }

                    isDestroyed = true;
                }
            }

            else if (direction == 2)  //check to the left
            {
                if (currentColumn > 0 && grid[currentRow , currentColumn - 1].IsVisited)
                {
                    if (grid[currentRow, currentColumn].LeftWall)
                    {
                        Destroy(grid[currentRow, currentColumn].LeftWall);
                    }

                    if (grid[currentRow, currentColumn - 1].RightWall)
                    {
                        Destroy(grid[currentRow, currentColumn-1].RightWall);
                    }

                    isDestroyed = true;
                }

            }

            else if (direction == 3)   //check to the right
            {
                if (currentColumn < columns -1 && grid[currentRow , currentColumn + 1 ].IsVisited)
                {
                    if (grid[currentRow, currentColumn].RightWall)
                    {
                        Destroy(grid[currentRow, currentColumn].RightWall);
                    }

                    if (grid[currentRow, currentColumn + 1].LeftWall)
                    {
                        Destroy(grid[currentRow, currentColumn + 1].LeftWall);
                    }

                    isDestroyed = true;
                }
            }

        }

    }

    void HuntersNeverRest()
    {

        scanComplete = true;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    //grid scan in order to find an unvisited cell that has a visited neighbor
                    if (!grid[i, j].IsVisited && VisitedNeighborCellsExist(i, j))
                    {
                        scanComplete = false;

                        currentRow = i;
                        currentColumn = j;
                        grid[currentRow, currentColumn].IsVisited = true;
                        DestroyAdjacentWall();
                        return;
                    }
                }
            }
        }
    }


