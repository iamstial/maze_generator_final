﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour
{

   
    public Text dialogueText;

    private Queue<string> sentences;


    void Start()
    {
        sentences = new Queue<string>() ;
    }


    public void StartDialogue (Dialogue dialogue){
        



        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();

        dialogueText.text = sentence;
    }

    public void EndDialogue()
    {
        Debug.Log("End of convo");
    }

    
}
