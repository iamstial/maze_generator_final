﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewDialogTrigger : MonoBehaviour
{
    [SerializeField] public GameObject customImage;
    [SerializeField] public GameObject customImage2;
    [SerializeField] public GameObject customImage3;

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("triggered");
        customImage.SetActive(true);
        customImage2.SetActive(true);
        customImage3.SetActive(true);

    }

    private void OnTriggerExit(Collider other)
    {
        customImage.SetActive(false);
        customImage2.SetActive(false);
        customImage3.SetActive(false);
    }
}
